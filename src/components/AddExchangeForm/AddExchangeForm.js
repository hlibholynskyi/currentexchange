import React, { Component } from 'react';
import './AddExchangeForm.css';
import axios from 'axios'

class AddExchangeForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allCurrencyData: [],
      baseCurrencyData: [],
      counterCurrencyData: [],
      counterSelect: 'BGN',
      baseSelect: 'BGN',
    }
  }

  componentDidMount() {
    axios.get('https://api.exchangeratesapi.io/latest')
      .then(res => {
        this.setState({allCurrencyData: Object.keys(res.data.rates)})
      })
  }

  addExchangePair = () => {
    axios.get(`https://api.exchangeratesapi.io/latest?base=${this.state.baseSelect}`)
      .then(res => {
        const { tableData } = this.props;
        tableData.push({base: res.data.base, counter: this.state.counterSelect, rate: res.data.rates[this.state.counterSelect]});
        this.props.addTableData(tableData)
      })

  };

  changeCounterSelect = (event) => {
    this.setState({counterSelect: event.target.value});
  };

  changeBaseSelect = (event) => {
    this.setState({baseSelect: event.target.value});
  };

  render() {
    return (
      <div className="add-exchange-form">
        <div>
          <div className="select-block">
            <p>Base currency</p>
            <div className="select-style">
              <select name="Counter" id="" onChange={this.changeBaseSelect}>
                {this.state.allCurrencyData.map((el, i)=> <option key={i}>{el}</option>)}
              </select>
            </div>
          </div>
          <div className="select-block">
            <p>Counter currency</p>
            <div className="select-style">
              <select name="Counter" id="" onChange={this.changeCounterSelect}>
                {this.state.allCurrencyData.map((el, i)=> <option key={i}>{el}</option>)}
              </select>
            </div>
          </div>
        </div>
        <button className="add-button" onClick={() => this.addExchangePair()}>Add exchange pair</button>
      </div>
    )
  }
}

export default AddExchangeForm;
