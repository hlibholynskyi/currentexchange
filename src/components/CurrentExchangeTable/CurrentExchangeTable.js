import React, { Component } from 'react';
import './CurrentExchangeTable.css';
import AddExchangeForm from "../AddExchangeForm/AddExchangeForm";

class CurrentExchangeTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableData: [],
    }
  }

  componentDidMount() {
    const tableData = localStorage.getItem('tableData') ? JSON.parse(localStorage.getItem('tableData')) : [];
    this.setState({tableData})
  }

  addTableData = (tableData) => {
    localStorage.setItem('tableData', JSON.stringify(tableData));
    this.setState({tableData})
  };

  removeTableData = (i) => {
    const { tableData } = this.state;
    tableData.splice(i, 1);
    localStorage.setItem('tableData', JSON.stringify(tableData));
    this.setState({tableData})
  };

  render() {
    return (
      <div className="current-exchange-table">
        <div className="table-block">
          <table>
            <thead className="table-header">
            <tr>
              <th>Base</th>
              <th>Counter</th>
              <th>Rate</th>
              <th/>
            </tr>
            </thead>
            <tbody>
            {
              this.state.tableData.length > 1 ? this.state.tableData.map((el, i) => {
                return (
                  <tr key={i}>
                    <td>{el.base}</td>
                    <td>{el.counter}</td>
                    <td>{el.rate}</td>
                    <td><i onClick={this.removeTableData} className="fa fa-trash delete-button" /></td>
                  </tr>
                )
              }) : null
            }
            </tbody>
          </table>
        </div>
        <AddExchangeForm addTableData={this.addTableData} tableData={this.state.tableData}/>
      </div>
    )
  }
}

export default CurrentExchangeTable;
