import React, { Component } from 'react';
import CurrentExchangeTable from './components/CurrentExchangeTable/CurrentExchangeTable';

class App extends Component {


  render() {
    return (
      <div className="App">
        <CurrentExchangeTable />
      </div>
    );
  }
}

export default App;
